#!/bin/bash

percentage=$1

# for every pdbID in argument
for pdbID in ${@:2};do
    pdbID=$(echo $pdbID | tr [:lower:] [:upper:])
    echo $pdbID

    # download source code of the $percentage% sequence similarity chain IDs webpage
    wget -O $pdbID"_"$percentage"_webpage.txt" "https://www.rcsb.org/pdb/explore/sequenceCluster.do?structureId="$pdbID"&entity=1&seqid="$percentage
    echo ""
    echo "extracting pdbIDs for this one"
    grep 'href="/pdb/explore.do?structureId=' $pdbID"_"$percentage"_webpage.txt" |awk '{ print $2 }' |grep -o '....$' > $pdbID"_"$percentage"_rejectpdbs.txt"
    if [ -d /storage/swastik/db/rejectpdblists/rejectpdblists_$percentage ];then
        cp $pdbID"_"$percentage"_rejectpdbs.txt" /storage/swastik/db/rejectpdblists/rejectpdblists_$percentage/
    else
        mkdir /storage/swastik/db/rejectpdblists/rejectpdblists_$percentage
        cp $pdbID"_"$percentage"_rejectpdbs.txt" /storage/swastik/db/rejectpdblists/rejectpdblists_$percentage/
    fi

    rm $pdbID"_"$percentage"_webpage.txt"
    echo $pdbID processing complete
    echo "---------------"
    echo ""
done
