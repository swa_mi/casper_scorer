#!/bin/bash
#PBS -N VAR_scoring
#PBS -J 1-VAR:1
#PBS -l exclude_node=yes
#PBS -e VAR/erroutfiles/^array_index^.err
#PBS -o VAR/erroutfiles/^array_index^.out

cd $PBS_O_WORKDIR
hostname

read -a array_swastik <<< $(cat "VAR")      # file with pathnames to all .cliq.gpdb files
cliqgpdb_name=${array_swastik[`expr $PBS_ARRAY_INDEX - 1`]}
#echo "after reading file"
echo $(date +"%F %T"): Sub-job started. Name of clique being scored in this sub-job is $cliqgpdb_name

# every line in cliqfilenames.txt has the name of a cliq.gpdb file, one of which will be scored in each subjob
# That name is the same as the $filevar here and hence would be the name of the directory
# where everything will be processed under this sub-job
filevar=$(basename $cliqgpdb_name .cliq.gpdb)
echo $"Subjob directory is "$filevar

querydir=VAR
logoutput=$(bash $querydir/pbsprocessing.sh $filevar 2>&1);printf "$logoutput" > $querydir/erroutfiles/$filevar.log

echo $(date +"%F %T"): Subjob ended. Exiting...
