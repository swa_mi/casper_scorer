import sys

# read pbs_template and split it at variables that need to be added. 
# Variables defined by the term VAR
# For changes that need to be made to the pbs script, make changes to the template

# Defining some variables and assigning parameters taken from command line arguments
pbs_template = sys.argv[1]      # the original template
cliqscount = sys.argv[2]        # taken from a count of the .cliq.gpdb files produced after ParseBack
workingdir = sys.argv[3]        # the working dir where this particular pdb is being scored. 
# Also is the outfol where all the .clique files will be placed later

if workingdir.endswith("/")!=True:
    pdbname=workingdir
    workingdir = workingdir + "/"
else:
    pdbname=workingdir[:-1]

def getFileNameWithoutExtension(path):
    # this function does exactly what it's name says (duh!)
    return path.split('\\').pop().split('/').pop().rsplit('.', 1)[0]

# Note: total number of cliqs is not equal to total number of groups in the file
# clique with all groups from same residue will not be considered 
# so total number of cliques is actually a little less than 
# the total number of chemical groups in the protein

with open(pbs_template, 'r') as pbstemplf:
    pbstemplf = (pbstemplf.read()).split("VAR")

pdbname_sans_ext = getFileNameWithoutExtension(pdbname)

# read rest of the variables from the command line arguments 
# and add them in between the split list called pbstemplf
pbstemplf = pbstemplf[0] + pdbname_sans_ext + \
            pbstemplf[1] + cliqscount + \
            pbstemplf[2] + workingdir + \
            pbstemplf[3] + workingdir + \
            pbstemplf[4] + workingdir + "cliqfilenamelist.txt" +\
            pbstemplf[5] + workingdir + \
            pbstemplf[6]

print(workingdir + pdbname_sans_ext)
with open(workingdir + pdbname_sans_ext + "_pbsscorer.sh", "w") as pbscriptf:
    pbscriptf.write(pbstemplf)


