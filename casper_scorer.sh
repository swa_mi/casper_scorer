#!/bin/bash

# getopts takes following parameters:
    # -n) size of cliques
    # -d) distance-cutoff for farthest group in the star
    # -r) rmsd cutoff of star-clique superimposition
    # -p) penalty for non-superimposition
    # -s) sampling size from the stars-database
    # -l) (Optional) (is true by default) to obtain log file

#default values
pdbfiles='scorethis'
size=7
dmax=20
rmsd=2
penalty=5
sampling=4000
maxCASPERcutoff=2
log=1


#check if getopts found any arguments at all
if ( ! getopts ":in:d:r:p:s:g:h" opt); then
    printf "Usage: `basename $0` options -i [input_file.pdb]\n \
            -i file with a list of input files, which are in pdb format\n \
            -n size\n  \
            -d distance cutoff for farthest clique in the stars\n \
            -r RMSD cutoff for star-clique superimposition\n \
            -p penalty for non-matches\n \
            -l 0/1 for generating log file (optional, default=1)\n \
            -s sampling size from the stars-database \n \
            -h for help \n";
	exit $E_OPTERROR;
fi

while getopts "i:n:d:r:p:s:g:h" opt; do
     case $opt in
        i) pdbfiles=$OPTARG;;
        n) size=$OPTARG;;
        d) dmax=$OPTARG;;
        r) rmsd=$OPTARG;;
        p) penalty=$OPTARG;;
        s) sampling=$OPTARG;;
        g) maxCASPERcutoff=$OPTARG;;
        h) printf "Usage: `basename $0` options -i [input_file.pdb]\n \
            -i input file in pdb format\n \
            -n size\n \
            -d distance cutoff for farthest clique in the stars\n \
            -r RMSD cutoff for star-clique superimposition\n \
            -p penalty for non-matches\n \
            -s sampling size from the stars-database \n \
            -l 0/1 for generating log file (optional, default=1)\n \
            -g max CASPER RMSD cutoff\n \
            -h for help \n";;
        \? ) echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :  ) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
        *  ) echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
     esac
done

shift $(($OPTIND - 1))

if [ "scorethis" == "scorethis"$pdbfiles ]
then
    echo "-i must be included with a specified file" >&2
    exit 1
elif [ "${pdbfiles:0:1}" == "-" ]; then
	printf "PDB filename shouldn't start with '-' \nPlease change the PDB filename or check your command again for missing PDB filename as argument to -i flag.\n"
	exit 1
fi

# all parameters are now known and processes can begin

###################################################################

logfilename=$(basename $pdbfiles .txt)

/home/swastik/bin/casper_scorer/scorethis.sh $pdbfiles $size $dmax $rmsd $penalty $sampling $maxCASPERcutoff >> $logfilename"_"$size"_"$dmax"_"$rmsd"_"$penalty"_"$sampling"_"$maxCASPERcutoff".log" 2>&1 & disown


#echo "/storage/swastik/scorethis.sh" $pdbfiles $size $dmax $rmsd $penalty $sampling" >>" $logfilename"_"$size"_"$dmax"_"$rmsd"_"$penalty"_"$sampling".log & disown"

